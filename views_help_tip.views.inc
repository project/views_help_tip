<?php

/**
 * @file
 * Contains views_help_tip\views_help_tip.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */

/**
* Implements hook_views_data().
*/
function views_help_tip_views_data() {

    $data['views']['table']['group'] = t('Custom Global');
    $data['views']['table']['join'] = [
      // #global is a special flag which allows a table to appear all the time.
      '#global' => [],
    ];

    $data['views']['help_tip'] = [
      'title' => t('Help tip area'),
      'help' => t('A help tip.'),
      'area' => [
        'id' => 'help_tip',
      ],
    ];

    return $data;
}
